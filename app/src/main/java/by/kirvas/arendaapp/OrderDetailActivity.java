package by.kirvas.arendaapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import by.kirvas.arendaapp.bean.Client;
import by.kirvas.arendaapp.dao.ServerDAO;
import by.kirvas.arendaapp.dao.TempLists;

public class OrderDetailActivity extends AppCompatActivity {

    private int boxes;
    public static String EXTRA_CUS = "customerId";
    private Client currClient;
    private List<Boolean> boxlist;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.order_detail_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case (R.id.make_order):
                ArrayList<Integer> ids = checkBoxes();
                ServerDAO.getInstance().sendCheckBoxes(ids);
                Intent intent = new Intent(this, ClientActivity.class);
                intent.putExtra(ClientActivity.EXTRA_CLI, currClient);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ServerDAO serverDAO = ServerDAO.getInstance();
       // currClient = serverDAO.loadClients().get((int)getIntent().getLongExtra(EXTRA_CUS, 0));
       //boxlist = serverDAO.loadPlaces(getResources().getString(R.string.link_places), currClient.getId());
        TempLists.generateList();
        currClient = TempLists.list.get(getIntent().getIntExtra(EXTRA_CUS, 0));
        boxlist = TempLists.loadPlaces(currClient.getId());
        currClient.setPlaces(boxlist.size());
        setTitle(currClient.getName());
        new DownloadImageTask((ImageView) findViewById(R.id.imageOrder))
                .execute(getResources().getString(R.string.link_map) + currClient.getMapId());
        addBoxes();
    }

    private void addBoxes(){
        //ArrayList<CheckBox> al = getCliBoxes(currClient.getPlaces(), getApplicationContext(), currClient.getPlaces());
        ArrayList<CheckBox> al = TempLists.getCusBoxes(currClient.getPlaces(), getApplicationContext());
        GridLayout layout = (GridLayout)findViewById(R.id.order_detail_layout);
        boxes = al.size();
        if (boxes == 0){
            Toast toast = Toast.makeText(this, "There is no places", Toast.LENGTH_LONG);
            toast.show();
        }
        for (int i = 0; i < boxes; i++){
            layout.addView(al.get(i));
        }
    }

    private ArrayList<Integer> checkBoxes(){
        ArrayList<Integer> ids = new ArrayList<>();
        for (int i = 0; i < boxes; i++){
            CheckBox box = (CheckBox)findViewById(i*13);
            if (box.isChecked()){
                ids.add(i);
            }
        }
        return ids;
    }

    private ArrayList<CheckBox> getCliBoxes(int id, Context context, int places){
        ArrayList<CheckBox> cblist = new ArrayList<>();
        for (int i = 0; i < boxlist.size(); i++){
            CheckBox checkBox = new CheckBox(context);
            checkBox.setId(i*13);
            checkBox.setEnabled(!boxlist.get(i));
            checkBox.setText("Place " + (i + 1));
            cblist.add(checkBox);
        }
        return cblist;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }


}
