package by.kirvas.arendaapp;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;

import by.kirvas.arendaapp.bean.Client;
import by.kirvas.arendaapp.dao.ServerDAO;

public class ClientMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, LocationListener {

    private GoogleMap myMap;
    private MapView myMapView;
    private View myView;
    private Context context;
    private List<Client> clients;
    private HashMap<String, Client> cliCoords = new HashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_map_layout, container, false);
        context = inflater.getContext();
        clients = MainActivity.clientsList;
        for (Client cli : clients) {
            if (cli.getParsedCoords() != null) {
                cliCoords.put(cli.getParsedCoords().toString(), cli);
            }
        }
        return myView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        myMapView = (MapView) myView.findViewById(R.id.map_fragment);
        if (myMapView != null) {
            myMapView.onCreate(null);
            myMapView.onResume();
            myMapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(context);
        myMap = googleMap;
        myMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        addMarkers(myMap);
        myMap.setMyLocationEnabled(true);
        CameraPosition position = CameraPosition.builder().target(new LatLng(53.899741, 27.555893)).zoom(10).bearing(0).tilt(0).build();
        myMap.moveCamera(CameraUpdateFactory.newCameraPosition(position));
        myMap.setOnInfoWindowClickListener(this);

    }

    private void addMarkers(GoogleMap myMap){

        for (Client cust : clients){
            if (cust.getParsedCoords() != null){
                myMap.addMarker((new MarkerOptions().position(cust.getParsedCoords()).title(cust.getName())));
            }
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);
        myMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        myMap.animateCamera(CameraUpdateFactory.zoomTo(15));

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent intent = new Intent(context, ClientActivity.class);
        intent.putExtra(ClientActivity.EXTRA_CLI, (long) cliCoords.get(marker.getPosition().toString()).getId()-1);
        startActivity(intent);
    }
}

