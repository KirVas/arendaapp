package by.kirvas.arendaapp.bean;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class Client implements Serializable{

    private int id;
    private String name;
    private String address;
    private double latitude;
    private double longitude;
    private String mapId;
    private String info;
    private String logo;
    private String phone;
    private String email;
    private double rating;
    transient private LatLng parsedCoords;

    public LatLng getParsedCoords() {
        return parsedCoords;
    }

    public void setParsedCoords(LatLng parsedCoords) {
        this.parsedCoords = parsedCoords;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private int places;

    public Client(int id, String name, String address, String mapId, int places, String info, String phone, String email, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.mapId = mapId;
        this.email = email;
        this.places = places;
        this.phone = phone;
        this.info = info;
        this.latitude = latitude;
        this.longitude = longitude;
        this.parsedCoords = new LatLng(latitude, longitude);
    }

    public Client(){}

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMapId() {
        return mapId;
    }

    public void setMapId(String mapId) {
        this.mapId = mapId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;

        Client client = (Client) o;

        if (id != client.id) return false;
        if (Double.compare(client.latitude, latitude) != 0) return false;
        if (Double.compare(client.longitude, longitude) != 0) return false;
        if (!name.equals(client.name)) return false;
        if (address != null ? !address.equals(client.address) : client.address != null) return false;
        if (mapId != null ? !mapId.equals(client.mapId) : client.mapId != null) return false;
        if (info != null ? !info.equals(client.info) : client.info != null) return false;
        if (logo != null ? !logo.equals(client.logo) : client.logo != null) return false;
        if (phone != null ? !phone.equals(client.phone) : client.phone != null) return false;
        if (email != null ? !email.equals(client.email) : client.email != null) return false;
        return parsedCoords != null ? parsedCoords.equals(client.parsedCoords) : client.parsedCoords == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + (address != null ? address.hashCode() : 0);
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (mapId != null ? mapId.hashCode() : 0);
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (logo != null ? logo.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (parsedCoords != null ? parsedCoords.hashCode() : 0);
        return result;
    }
}
