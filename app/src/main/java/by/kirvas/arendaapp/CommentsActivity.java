package by.kirvas.arendaapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import by.kirvas.arendaapp.bean.Client;
import by.kirvas.arendaapp.dao.ServerDAO;

public class CommentsActivity extends AppCompatActivity {

    static String EXTRA_CUST = "customerchik";
    Client client;
    List<String> listcom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        client = (Client)getIntent().getSerializableExtra(EXTRA_CUST);
        listcom =  ServerDAO.getInstance().loadComments(getResources().getString(R.string.link_comments), client.getId());
        int t = listcom.size();
        String[] coms = new String[t];
        for (int i = 0; i < t; i++){
            coms[i] = listcom.get(i);
        }
        if (coms.length != 0){
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, coms);
            ((ListView)findViewById(R.id.comment_list)).setAdapter(adapter);
            Toast.makeText(getApplicationContext(), String.valueOf(coms.length), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Error while filling list", Toast.LENGTH_SHORT).show();
        };
    }

    public void onSendCommentClick(View view){
        String nick = ((EditText)findViewById(R.id.nicktext)).getText().toString();
        String comment = ((EditText)findViewById(R.id.comtext)).getText().toString();
        if(nick.equals("") || nick.isEmpty() || comment.equals("") || comment.isEmpty()){
            Toast.makeText(getApplicationContext(), "Fill both fields", Toast.LENGTH_SHORT).show();
        } else {
            sendComment(nick, comment);
            view.setEnabled(false);
        }

    }

    private void sendComment(String nick, String comment){
        ServerDAO serverDAO = ServerDAO.getInstance();
        serverDAO.sendComment(getResources().getString(R.string.link_comments), nick, comment, client.getId());
    }
}
