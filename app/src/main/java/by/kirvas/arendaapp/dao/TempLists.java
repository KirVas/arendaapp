package by.kirvas.arendaapp.dao;

import android.content.Context;
import android.widget.CheckBox;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import by.kirvas.arendaapp.bean.Client;

import static android.media.CamcorderProfile.get;

public class TempLists {
    private  TempLists instance = new TempLists();

    public static List<Client> list = new ArrayList<>();
    public static List<String> nameList = new ArrayList<>();

    public TempLists getInstance() {
        return instance;
    }

    public static void generateNameList(){
        generateList();
        for (int i = 0; i < 10; i++){
            nameList.add(list.get(i).toString());
        }
    }

    public static void generateList(){
        for (int i = 0; i < 10; i++){
            list.add(new Client(i, "name" + i, "adress" + i, "map" +  i, i, "info" + i, "phone" + i, "email" + i + "@dot.com", 53.4788, 27.87574));
        }
        list.get(5).setRating(((double)((int)(Math.random()*100)))/10);
        list.get(2).setRating(((double)((int)(Math.random()*100)))/10);
        list.get(4).setRating(((double)((int)(Math.random()*100)))/10);
        list.get(3).setParsedCoords(new LatLng(53.950333, 27.604042));
        list.get(7).setParsedCoords(new LatLng(53.946140, 27.611509));
    }

    public static ArrayList<CheckBox> getCusBoxes(int id, Context context){
        ArrayList<CheckBox> cblist = new ArrayList<>();
        int t = list.get(id).getPlaces();
        for (int i = 0; i < t; i++){
            CheckBox checkBox = new CheckBox(context);
            checkBox.setId(i*131);
            checkBox.setText("Place " + i);
            cblist.add(checkBox);
        }
        return cblist;
    }

    public static List<Boolean> loadPlaces(int id){

        List<Boolean> places = new ArrayList<>();
        int i = id;
            while (i > 0){
                i--;
                boolean add = (Math.random()*10 < 5);
                places.add(add);
            }

        return places;
    }
}
