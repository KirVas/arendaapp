package by.kirvas.arendaapp;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.List;

import by.kirvas.arendaapp.bean.Client;

public class ClientsListFragment extends ListFragment {

    private LayoutInflater inflater;
    private List<Client> clients;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        this.inflater = inflater;
        clients = MainActivity.clientsList;
//        int t = clients.size();
//        String[] names = new String[t];
//        for (int i = 0; i < t; i++){
//            names[i] = clients.get(i).toString();
//        }
//        if (names.length != 0){
//            ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(), android.R.layout.simple_list_item_1, names);
//            setListAdapter(adapter);
//        } else {
//            Toast.makeText(inflater.getContext(), "Error while loading places. \n Please, check your network connection.", Toast.LENGTH_SHORT).show();
//        }
        CliListAdapter adapter = new CliListAdapter();
        setListAdapter(adapter);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onAttach(Activity activity){
        super.onAttach(activity);
    }

    public void onListItemClick(ListView l, View v, int position, long id){
        Intent intent = new Intent(inflater.getContext(), ClientActivity.class);
        intent.putExtra(ClientActivity.EXTRA_CLI, clients.get(position));
        startActivity(intent);
    }

    private class CliListAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return clients.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.clilist_item, null);

            ImageView listedLogo = (ImageView)convertView.findViewById(R.id.listedLogo);
            TextView listedName = (TextView)convertView.findViewById(R.id.listedName);
            TextView listedAddress = (TextView)convertView.findViewById(R.id.listedAddress);
            TextView listedRating = (TextView)convertView.findViewById(R.id.listedRating);
            Client client = clients.get(position);
            listedName.setText(client.getName());
            listedAddress.setText(client.getAddress());
            listedRating.setText(client.getRating() + "/10");

            //new DownloadImageTask(listedLogo)
             //       .execute(getResources().getString(R.string.link_map) + client.getLogo());
            return convertView;
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}

