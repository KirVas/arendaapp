package by.kirvas.arendaapp;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import java.util.List;

import by.kirvas.arendaapp.bean.Client;
import by.kirvas.arendaapp.dao.ServerDAO;
import by.kirvas.arendaapp.dao.TempLists;

public class MainActivity extends AppCompatActivity {

    //public static List<Client> clientsList = ServerDAO.getInstance().loadClients();
    public static List<Client> clientsList = TempLists.list;
    private ClientsListFragment clf = new ClientsListFragment();
    private ClientMapFragment cmf = new ClientMapFragment();
    private FragmentTransaction ft;
    private boolean isList;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_list:
                    if (!isList){
                        prepareList();
                    }
                    return true;
                case R.id.navigation_map:
                    if (isList){
                        prepareMap();

                    }
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TempLists.generateList();
        BottomNavigationView navigation = (BottomNavigationView)findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        prepareList();
        isList = true;

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)){

            }else{
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 666);
            }
        }
    }

    private void prepareList(){
        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, clf);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
        isList = true;
    }

    private void prepareMap(){
        ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, cmf);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
        isList = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        //clientsList = ServerDAO.getInstance().loadClients();
    }
}
