package by.kirvas.arendaapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import by.kirvas.arendaapp.bean.Client;
import by.kirvas.arendaapp.dao.ServerDAO;

public class ClientActivity extends AppCompatActivity {

    public static String EXTRA_CLI = "clientPr";
    private Client currCl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        currCl = (Client)getIntent().getSerializableExtra(EXTRA_CLI);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(currCl.getName());
        setSupportActionBar(toolbar);
        final ServerDAO dao = ServerDAO.getInstance();
        final Intent intent = new Intent(ClientActivity.this, CommentsActivity.class);
        intent.putExtra(CommentsActivity.EXTRA_CUST, currCl);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    startActivity(intent);
//                    List<Client> list = dao.loadClients(getResources().getString(R.string.link_clients));
//                    ((TextView)findViewById(R.id.textCusAbout)).setText(list.get(0).getName());
                } catch (Exception e){
                    ((TextView)findViewById(R.id.textCusAbout)).setText(e.toString());
                }
            }
        });
        FloatingActionButton fabOrd = (FloatingActionButton) findViewById(R.id.fabOrd);
        fabOrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    Intent inn = new Intent(getApplicationContext(), OrderDetailActivity.class);
                    inn.putExtra(OrderDetailActivity.EXTRA_CUS, currCl.getId());
                    startActivity(inn);
                } catch (Exception e){
                    ((TextView)findViewById(R.id.textCusAbout)).setText(e.toString());
                }
            }
        });
        setUpInfo();
    }

    private void setUpInfo(){
        ((TextView)findViewById(R.id.textCusName)).setText(currCl.getName());
        ((TextView)findViewById(R.id.textCusAbout)).setText(currCl.getInfo());
        ((TextView)findViewById(R.id.textCusContacts)).setText(currCl.getAddress() + "\n" +
                                                                currCl.getEmail() + "\n" + currCl.getPhone());
    }

}
