package by.kirvas.arendaapp.dao;

import android.content.res.Resources;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import by.kirvas.arendaapp.R;
import by.kirvas.arendaapp.bean.Client;
import by.kirvas.arendaapp.bean.Dish;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ServerDAO {

    private Resources resources = Resources.getSystem();

    public static ServerDAO instance = new ServerDAO();

    public static ServerDAO getInstance() {
        return instance;
    }

    private ServerDAO(){

    }

    private URL currAdr;
    private String result;

    public void sendCheckBoxes(ArrayList<Integer> ids){

    }

    public List<Client> loadClients(){
        String url = resources.getString(R.string.link_clients);
        List<Client> list = new ArrayList<>();
        createConnection(url, NetWorkRequester.METHODS.GetClients, null);
        try {
            JSONObject jobj = new JSONObject(result);
            JSONObject cli;
            int i = 0;
            while (jobj.getJSONObject(String.valueOf(i)) != null){
                cli = jobj.getJSONObject(String.valueOf(i));
                i++;
                list.add(new Client(cli.getInt("id"), cli.getString("client_name"),
                        cli.getString("logo"), cli.getString("map"), cli.getInt("id"),
                        cli.getString("description"), cli.getString("phone"),
                        cli.getString("phone"), cli.getDouble("latitude"), cli.getDouble("longitude")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Client loadOneClient(int id){
        String url = resources.getString(R.string.link_clients) + "/" + id;
        createConnection(url, NetWorkRequester.METHODS.GetClients, null);
        Client client = new Client();
        try {
            JSONObject jobj = new JSONObject(result);
            JSONObject cli;
            int i = 0;
            while (jobj.getJSONObject(String.valueOf(i)) != null){
                cli = jobj.getJSONObject(String.valueOf(i));
                i++;
                new Client(cli.getInt("id"), cli.getString("client_name"),
                        cli.getString("logo"), cli.getString("map"), cli.getInt("id"),
                        cli.getString("description"), cli.getString("phone"),
                        cli.getString("phone"), cli.getDouble("latitude"), cli.getDouble("longitude"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return client;
    }

    private void createConnection(String url, NetWorkRequester.METHODS method, String meta) {
        try {
            currAdr = new URL(url);
            Thread thread = new Thread(new NetWorkTread(meta, method));
            thread.start();
            thread.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public List<Boolean> loadPlaces(String url, int id){
        createConnection(url + id, NetWorkRequester.METHODS.GetPlaces, null);
        List<Boolean> places = new ArrayList<>();
        try {
            JSONArray pll = (new JSONObject(result)).getJSONArray("places");
            JSONObject place;
            int i = 0;
            while (pll.getJSONObject(i) != null){
                place = pll.getJSONObject(i);
                i++;
                boolean add = (place.getInt("occupied") != 0);
                places.add(add);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return places;
    }

    public String sendPlaces(String url, int id, int places){
        createConnection(url + id + "/" + places, NetWorkRequester.METHODS.SendPlaces, null);
        return result;
    }

    public List<Dish> loadDishes(String url, int id){
        createConnection(url + id, NetWorkRequester.METHODS.GetPlaces, null);
        List<Dish> dishes = new ArrayList<>();
        try {
            JSONArray dshl = (new JSONObject(result)).getJSONArray("places");
            JSONObject place;
            int i = 0;
            while (dshl.getJSONObject(i) != null){
                place = dshl.getJSONObject(i);
                i++;
             //   Dish add = (place.getInt("occupied") != 0);
             //   dishes.add(add);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dishes;
    }

    public void sendOrder() {

    }

    public void sendComment(String url, String nick, String comment, int id){
        String toSendJSON = "{ \"name\":\"" + nick + "\",\"text\":\"" + comment + "\"}";
        createConnection(url + id, NetWorkRequester.METHODS.SendComment, toSendJSON);
    }

    public String sendClient(String url, Client cli){
        String toSendJSON = "{\"client_name\":\"" + cli.getName() +
                "\",\"description\":\"" + cli.getInfo() + "\",\"logo\":\"" +
                cli.getLogo() + "\",\"map\":\"" + cli.getMapId() + "\",\"phone\":\""
                + cli.getPhone() + "\",\"latitude\":\"" + cli.getLatitude() +
                "\",\"longitude\":\"" + cli.getLongitude() + "\"}";
        createConnection(url, NetWorkRequester.METHODS.SendClient, toSendJSON);
        return result;
    }

    public List<String> loadComments(String url, int id){
        List<String> comments = new ArrayList<>();
        createConnection(url + id, NetWorkRequester.METHODS.GetComments, null);
        try {
            JSONArray cl = (new JSONObject(result)).getJSONArray("comments");
            JSONObject comment;
            int i = 0;
            while (cl.getJSONObject(i) != null){
                comment = cl.getJSONObject(i);
                i++;
                comments.add(comment.getString("name") + " :   " + comment.getString("text"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return comments;
    }



    class NetWorkTread implements Runnable{

        String meta = null;
        NetWorkRequester.METHODS method;

        NetWorkTread(String meta, NetWorkRequester.METHODS method){
            this.meta = meta;
            this.method = method;
        }

        @Override
        public void run() {
            try{
                OkHttpClient client = new OkHttpClient();
                NetWorkRequester nwr = new NetWorkRequester();
                Request request = nwr.buildRequest(method, currAdr, meta);
                Response response = client.newCall(request).execute();
                result = response.body().string();
            }catch(Exception e){
                result = "Network Failed";
                e.printStackTrace();
            }
        }

    }

//    class NWTask extends AsyncTask<Void, Void, Void> implements Runnable {
//
//        String result;
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            try{
//                OkHttpClient client = new OkHttpClient();
//
//                Request request = new Request.Builder()
//                        .url(currAdr)
//                        .build();
//
//                Response response = client.newCall(request).execute();
//                result = response.body().string();
//                return null;
//            }catch(Exception e){
//                result = e.toString();
//                e.printStackTrace();
//                return null;
//            }
//
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            toShow = result;
//            super.onPostExecute(aVoid);
//        }
//
//        @Override
//        public void run() {
//
//        }
//    }
}
