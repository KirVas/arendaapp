package by.kirvas.arendaapp.dao;

import java.net.URL;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

public class NetWorkRequester {

    public Request buildRequest(METHODS method, URL url, String meta){
        Request request = null;

        switch (method){
            case GetClients:
                request = new Request.Builder().url(url).build();
                break;
            case GetComments:
                request = new Request.Builder().url(url).build();
                break;
            case SendComment:
                RequestBody body1 = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), meta);
                request = new Request.Builder().url(url).post(body1).build();
                break;
            case SendClient:
                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), meta);
                request = new Request.Builder().url(url).post(body).build();
                break;
            case GetPlaces:
                request = new Request.Builder().url(url).build();
                break;
            case SendPlaces:
                request = new Request.Builder().url(url).post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), " ")).build();
                break;
            case GetMenu:
                request = new Request.Builder().url(url).post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), " ")).build();
                break;
        }

        return request;
    }

    enum METHODS{
        GetClients, GetComments, SendClient, GetPlaces, SendPlaces, SendComment, GetMenu
    }

}
